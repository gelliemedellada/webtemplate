<?php
  /* Start Set Cookie Params*/
  $maxlifetime = 0;
  if(PHP_VERSION_ID < 70300) {
    session_set_cookie_params($maxlifetime, '; samesite=lax', $_SERVER['HTTP_HOST'], true, true);
  } else {
    session_set_cookie_params([
      'lifetime' => $maxlifetime,
      'secure' => true,
      'httponly' => true,
      'samesite' => 'lax'
    ]);
  }
  /* End Set Cookie Params*/

  session_start();
  $token = session_id();

  $rootDR = str_replace('\\', '/', substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT'])));
  define('_DR_', substr($rootDR,0,1)!='/'?'/'.$rootDR:$rootDR );
  define('_DRS_', __DIR__.'/src');

/* Start Validate Referer Information */
  $allowed_host = array('mck-phpdev-02.teleperformanceusa.com','mck-webwfm-01.teleperformanceusa.com','mck-webwfm-02.teleperformanceusa.com','mck-webwfm-02.teleperformanceusa.com', '127.0.0.1');

  include_once(_DRS_.'/config.php');

  if($cfg['maintenance']=='true'){
        include_once(_DRS_.'/builder/maintenance.php');
  }else{
    if(isset($_SERVER['HTTP_REFERER'])){
        $host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        if(in_array($host, $allowed_host)){          
          include_once(_DRS_.'/router.php');  
        }
      }else{
        if(in_array($_SERVER['SERVER_NAME'], $allowed_host)){
          include_once(_DRS_.'/router.php'); 
        }else{
          header("HTTP/1.1 401 Not Authorized" );
          exit;        
        }
      }
  }
/* End Validate Referer Information */
  
?>