<?php 
if(!isset($token) AND !isset($_GET['token']) AND !isset($_POST['token'])){header('location:../../');exit();}

include_once(__DIR__."/functions.php");

if(!session_id()){session_start();}
  /* Accepts parameters from login form */
  $authSanitizer = new Sanitizer();
  $loginCheck = $authSanitizer->isUserName($_POST['login']);

  if($loginCheck > 0){
    $error = "SQL Injection attempt detected.";
    unset($_POST['login']);unset($_POST['password']);
    include_once(_DRS_.'/pages/login.php');
    exit();
  }
  $login = htmlentities($_POST['login'], ENT_QUOTES, 'UTF-8');
  $password = $_POST['password'];

  if(!isset($_SESSION['attempt']) OR (isset($_SESSION['lockedOutTime']) AND time() >= strtotime("+30 minutes", $_SESSION['lockedOutTime']))){
    $_SESSION['attempt'] = 0;
  }else{
    $_SESSION['attempt'] = $_SESSION['attempt'];
  }

$lockOut = "<center>Access has been restricted due to multiple failed login attempts. Please wait 30 minutes to try again or contact <strong>BARC</strong> for assistance.</center>";
$accountLockOut = "<center>This account is currently locked out. Please contact <strong>BARC</strong> for assistance.</center>";
$errLogin = "Login attempt failed";

if($_SESSION['attempt']>=0){

  $mckQADemo = new mckQAConn();
  $mckQADemo = $mckQADemo->tryConnect();
  $dbDemo = $mckQADemo->prepare('SELECT TOP 1 password, username, role, emp_ident, emp_name, emp_fname, emp_email, desc_grade, last_attempt, attempts FROM wf_demo_logins (NOLOCK) WHERE username=:login');
  $dbDemo->bindparam(':login', $login);
  $dbDemo->execute();
  $dbDemo = $dbDemo->fetch();

  if($dbDemo['username'] != ''){

    $update_now = date('Y-m-d h:i:s');

    if(sha1($password) == $dbDemo['password'] AND $dbDemo['password'] < 3){

      session_regenerate_id();
      $_SESSION['login']=1;
      $_SESSION['username'] = $dbDemo['username'];
      $_SESSION['role'] = $dbDemo['role'];
      $_SESSION['ident'] = $dbDemo['emp_ident'];
      $_SESSION['email'] = $dbDemo['emp_email'];
      $_SESSION['name'] = $dbDemo['emp_name'];
      $_SESSION['emp_fname'] = $dbDemo['emp_fname'];
      $_SESSION['desc_grade'] = $dbDemo['desc_grade'];
      $_SESSION['position'] = $dbDemo['desc_grade'];
      $_SESSION['group'] = 'BARC';
      $_SESSION['token'] = session_id();

      $dbDemoUpdate = $mckQADemo->prepare('UPDATE wf_demo_logins SET last_attempt = :update_now, attempts = 0 WHERE username=:login');
      $dbDemoUpdate->bindparam(':update_now', $update_now);
      $dbDemoUpdate->bindparam(':login', $login);
      $dbDemoUpdate->execute();

    }else{
        $_SESSION['attempt'] = $_SESSION['attempt'] + 1;
        $attempt_cnt = $dbDemo['attempts'] + 1;
        $dbDemoUpdate = $mckQADemo->prepare('UPDATE wf_demo_logins SET last_attempt = :update_now, attempts =:attempt_cnt WHERE username=:login');
        $dbDemoUpdate->bindparam(':update_now', $update_now);
        $dbDemoUpdate->bindparam(':attempt_cnt', $attempt_cnt);
        $dbDemoUpdate->bindparam(':login', $login);
        $dbDemoUpdate->execute();

        if($attempt_cnt < 3){
          $error = $errLogin;          
        }else{
          if($_SESSION['attempt'] >= 3){
            $error = $lockOut;
            $_SESSION['lockedOutTime'] = time();
          }else{
            $error = $accountLockOut;        
          }
        }
    }
  }else{
  /* Check if user is on authorized list */
  $mckQA = new mckQAConn();
  $mckQA = $mckQA->tryConnect();
  $dbCheck = $mckQA->prepare('SELECT TOP 1 username, role, emp_ident, emp_name, emp_fname, emp_email, desc_grade, position_code_group FROM vw_users (NOLOCK) WHERE username=:login');
  $dbCheck->bindparam(':login', $login);
  $dbCheck->execute();
  $dbCheck = $dbCheck->fetch();
  if($dbCheck['username']==''){
      /* returns error message when user is not authorized */
      // $error = 'Login attempt failed: User does not exists';
        $_SESSION['attempt'] = $_SESSION['attempt'] + 1;
        if($_SESSION['attempt'] >= 3){
          $error = $lockOut;
          $_SESSION['lockedOutTime'] = time();
        }else{
          $error = $errLogin;
        }
   }else {
        /* CCMS Authentication */
        $base_url = 'https://www.seadc.ccms.teleperformance.com/authentication';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_USERPWD, $login.":".$password);
        curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
            if ($status_code==200) {
              /* Assign session variables if user is authorized, variables comes from authorized users table in MySQL */
              session_regenerate_id();
              $_SESSION['login']=1;
              $_SESSION['username'] = $dbCheck['username'];
              $_SESSION['role'] = $dbCheck['role'];
              $_SESSION['ident'] = $dbCheck['emp_ident'];
              $_SESSION['email'] = $dbCheck['emp_email'];
              $_SESSION['name'] = $dbCheck['emp_name'];
              $_SESSION['emp_fname'] = $dbCheck['emp_fname'];
              $_SESSION['desc_grade'] = $dbCheck['desc_grade'];
              $_SESSION['position'] = $dbCheck['position_code_group'];
              $_SESSION['group'] = getGroup($dbCheck['emp_ident'], $cfg['target']);
              $_SESSION['token'] = session_id();
              session_write_close();
            }else{
              /* Returns error when user enters wrong password */
              $_SESSION['attempt'] = $_SESSION['attempt'] + 1;
              if($_SESSION['attempt'] >= 3){
                $error = $lockOut;
                $_SESSION['lockedOutTime'] = time();
              }else{
                $error = $errLogin;
              }
            }
        }
    }
}
    if(isset($error)){

      logAction($login, 'login attempt', 'login failed');
      include_once(_DRS_."/pages/login.php");

    }else{

      $returnTo = $_POST['returnTo']!=""?'/'.urldecode($_POST['returnTo']):'';
      $checkReturnAddress = $returnTo==''?0:$authSanitizer->isFlagged($returnTo);
      $returnTo = getWelcomeMessage()=='yes'?('/welcome'.($_POST['returnTo']==""?'':'?return='.urlencode($returnTo))):$returnTo;

      if($checkReturnAddress == 0){
        unset($_SESSION['attempt']);
        header('location: '._DR_.$returnTo);
      }else{
        unset($_SESSION['attempt']);
        header('location: '._DR_);
      }

      logAction($login, 'login attempt', 'access granted');

    }
?>


