<?php

require(__DIR__.'/phpmailer/src/PHPMailer.php');
require(__DIR__.'/phpmailer/src/SMTP.php');
require(__DIR__.'/phpmailer/src/Exception.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class sendMail{
    public $from    = 'barcreports@teleperformanceusa.com';
    public $to      = null;
    public $cc      = null;
    public $subject = null;
    public $body    = null;
    
    public function sendFailedEmail(){
        $this->mailer = new PHPMailer(true);
        $this->mailer->IsSMTP();
        // $this->mailer->SMTPDebug = SMTP::DEBUG_SERVER;
        if(getRealHost() == '127.0.0.1' OR getRealHost() == 'localhost'){
            $this->mailer->Host = 'localhost';
        }else{
            $this->mailer->Host = 'smtp.teleperformanceusa.com';
        }

        try {
            $this->mailer->setFrom('barcreports@teleperformanceusa.com','WFM Reports Tracker Notification System');
            $emails = explode(';',$this->to);
            if(count($emails) > 0){
                foreach ($emails as $key => $email) {
                    if(filter_var($email,FILTER_VALIDATE_EMAIL)){
                        $this->mailer->addAddress($email);
                    }
                }
            } else{
                $this->mailer->addAddress($this->to);
            }
            if(isset($this->cc)){
                $this->mailer->AddCC($this->cc);
            }
            if(isset($this->bcc)){
                $this->mailer->addBCC($this->bcc);
            }
            $this->mailer->Subject = $this->subject;
            $this->mailer->msgHTML($this->body);
            if (!$this->mailer->send()) {
                $sent = 'Mailer Error: ' . $this->mailer->ErrorInfo.' ';
            } else {
                $sent = true;
            }
            
        } catch (Exception $e) {
            $sent = 'Mailer Error: ' . $this->mailer->ErrorInfo.' ';       
        }
            return $sent;
    }
}

?>