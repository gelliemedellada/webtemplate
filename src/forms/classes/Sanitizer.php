<?php

class Sanitizer{
	public $resultType;
	
	function __construct($resultType = '0') {
		if($resultType == '0'){
			$this->type = 'boolean';
		}elseif($resultType == '1'){
			$this->type = 'string';
		}else{
			$this->type = 'text';
		}
	}

	function isNumber($str){
		$pattern = '/[^0-9]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isFloat($str){
		$pattern = '/[^0-9.]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isUserName($str){
		$pattern = '/[^a-zA-Z0-9.]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isLetters($str){
		$pattern = '/[^a-zA-Z]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isAlphaNum($str){
		$pattern = '/[^a-zA-Z0-9\\s]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isPath($str){
		$pattern = '/[^a-zA-Z0-9_+]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isValidReturn($str){
		$pattern = '/[^a-zA-Z0-9_+/\?&=]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isFlagged($str){
		$pattern = '/[%<>]/i';
		if(preg_match_all($pattern, $str, $matches, PREG_OFFSET_CAPTURE)){
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}else{
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}
		return $return;
	}
	function isDate($str){
		$pattern = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
		if(preg_match($pattern,$str)) {
			$return = $this->type=='boolean'?0:($this->type=='string'?'true':'Valid input');
		}else {
			$return = $this->type=='boolean'?1:($this->type=='string'?'false':'Invalid input');
		}
		return $return;
	}

}