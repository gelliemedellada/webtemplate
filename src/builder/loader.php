<style>
.loading-screen{display: flex; justify-content:center;align-items:center;flex-direction:row;min-height:100vh;position:fixed;min-width:100vw;backdrop-filter:blur(4px);z-index:9999;background: rgba(255,255,255,0.25)}
.delay-0s{animation-delay: 0.1s;}
.delay-1s{animation-delay: 0.2s;}
.delay-2s{animation-delay: 0.3s;}
.delay-3s{animation-delay: 0.4s;}
.delay-4s{animation-delay: 0.5s;}
.delay-5s{animation-delay: 0.6s;}
.delay-6s{animation-delay: 0.7s;}
</style>
<div class="loading-screen">                                        
  <div class="spinner-grow text-muted"></div>
  <div class="spinner-grow text-dark delay-0s"></div>
  <div class="spinner-grow text-primary delay-1s"></div>
  <div class="spinner-grow text-success delay-2s"></div>
  <div class="spinner-grow text-info delay-3s"></div>
  <div class="spinner-grow text-warning delay-4s"></div>
  <div class="spinner-grow text-danger delay-5s"></div>
  <div class="spinner-grow text-secondary delay-6s"></div>
</div>