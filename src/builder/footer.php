<footer class="main-footer text-sm">
  Copyright &copy; <?php echo date('Y')=='2021'?date('Y'):"2021-".date('Y'); ?> by <strong>BARC</strong>.
  All rights reserved.
</footer>