<style>
  * {
    padding: 0;
    margin: 0;
  }
  #operation_error_background {
    position: fixed;
    *position: absolute;
    top: 0;
    left: 0;
    z-index:3001;
    width: 100%;
    height: 100%;
    background: #FFFFFF;
    background: #2D2D2D;
    opacity: .90;
    filter: alpha(opacity=90);
  }
  #operation_error {
    position: fixed;
    *position: absolute;
    top: 50px;
    right: 10%;
    z-index:3002;
    width: 80%;
    background: #FFFFFF;
    color: #000000;
    box-shadow: 0 0 30px #000;
  }
  #operation_error h1 {
    padding: 10px;
    background-color: #AF0A0A;
    color: #FFFFFF;
    font-family: sans-serif;
    font-size: 24px;
    font-weight: bold;
    letter-spacing: 1px;
  }
  #operation_error h1.state_error {
    background-color: #666666;
  }
  #operation_error h1 button {
    border: 1px solid #999999;
    background: #EFEFEF;
    color: #000000;
    font-weight: bold;
    text-align: center;
    font-size: 10px;
    height: 18px;
    width: auto;
    cursor: pointer;
    line-height: 18px;
    padding: 0 10px;
    position: relative;
    top: 5px;
    right: 5px;
    float: right;
    font-variant: normal;
    letter-spacing: 0;
    font-family: "Arial", "sans-serif";
    text-decoration: none;
    *overflow: visible; /* http://jehiah.cz/a/button-width-in-ie */
  }
  #operation_error h1 button:hover, #operation_error h1 button:focus {
    color: #999999;
  }
  #operation_error p {
    margin: 20px;
    font-family: sans-serif;
    font-size: 14px;
    font-weight: bold;
  }
  #operation_error p.title {
    color: #AF0A0A;
    font-size: 16px;
  }
  #operation_error p.state_error {
    color: #000000;
  }
  #operation_error p a {
    color: #336699;
    text-decoration: none;
  }
  #operation_error p a:hover, #operation_error p a:focus {
    color: #407FBF;
  }
  #operation_error ul {
    list-style-type: none;
    padding: 0 20px 20px 20px;
  }
  #operation_error ul li p {
    padding: 2px 0;
    margin: 0;
    font-size: 12px;
    font-weight: normal;
  }
  #operation_error ul li p span {
    padding: 0;
    margin: 0 4px 0 0;
    color: #656565;
  }
  #get_me_out{
    padding: 0.5rem 1rem;
    cursor: pointer;
    font-size: 105%;
  }
  .error_msg{
    border: 1px dotted #3e3e3e;
    background: #ededed;
    margin: 1rem;
  }
</style>
<title>Error</title>
<div id="operation_error_background">&nbsp;</div>
<div id="operation_error">
  <h1>Error</h1>
  <p class="title" style="line-height: 1.35;font-size:1.25rem;">
    The requested operation could not continue. To restart your operation, click <a href="<?php echo _DR_; ?>/">here</a>. If you continue to experience difficulties, please contact <a href="mailto:barcreports@teleperformance.com">BARC</a>.
  </p>
  <p>
    <button id="get_me_out">Get me out of here</button>
  </p>
  <div class="error_msg">
    <p>
      If this is a persistent error, please send an email to <a href="mailto:barcreports@teleperformanceusa.com">barcreports@teleperformanceusa.com</a> and copy the information below.
    </p>
    <p>
      URL: <span style="font-weight: 300;"><?php echo isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'Unknown'; ?></span>
      <br>Time: <span style="font-weight: 300;"><?php echo date("Y-m-d h:i:s A"); ?></span>
      <?php
        if(isset($_GET) AND isset($_POST)){
          $json_data = array_merge($_GET, $_POST);
        }elseif(isset($_GET)){
          $json_data = $_GET;
        }elseif(isset($_POST)){
          $json_data = $_POST;
        }else{
          $json_data = "null";
        }

        if($json_data!="null"){

          $conn = new mckQAConn();
          $conn = $conn->tryConnect();
          $nom_date = date('Y-m-d h:i:s A');
          
          $url_referer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'Unknown';
          $username = isset($_SESSION['username'])?$_SESSION['username']:$_SERVER['REMOTE_ADDR'];
          
          // $query = $conn->query("INSERT INTO [dbo].[wf_user_logs] ([datetime],[username],[action],[remarks]) VALUES ('{$nom_date}','{$username}','error','Referer: {$url_referer}\nPayload: ".json_encode($json_data)."')");
        
          echo "<br>Payload: <span style=\"font-weight: 300;\">".base64_encode(json_encode($json_data))."</span>";
        }
      ?>
    </p>
  </div>
</div>
<script type="text/javascript">
  var get_me_out = document.getElementById("get_me_out");
  get_me_out.onclick = function (e) { location.href = "<?php echo _DR_; ?>/"; }
</script>