<html>
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link rel="icon" type="image/png" href="<?php echo _DR_?>/dist/img/TP-logo.ico"> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>WFM Reports Tracker</title>
  <link rel="stylesheet" href="<?php echo _DR_.'/dist/css/login.css'?>">
  <link rel="stylesheet" href="<?php echo _DR_.'/dist/css/fontawesome/fontawesome.min.css'?>">
</head>

<body>
  <div class="container">
    <div class="container__forms">
      <div class="form">
        <form class="form__sign-in">
          <svg width="102" height="106" viewBox="0 0 62 66" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M39.9 0H2.3C1 0 0 1 0 2.3V13.5C0 14.4 0.2 14.9 0.6 15.4C3.8 18.6 10.4 25.2 13.6 28.4C13.8 28.6 14.1 28.5 14.1 28.2C14.1 24.9 14.1 15 14.1 15C14.1 14.7 13.9 14.5 13.6 14.5H3.3C2.6 14.5 2.1 14 2.1 13.3V3.4C2.1 2.7 2.6 2.2 3.3 2.2H37.8C38.5 2.2 39 2.7 39 3.4V13.3C39 14 38.5 14.5 37.8 14.5H27.4C27.1 14.5 26.9 14.7 26.9 15V53.5C26.9 54.2 26.4 54.7 25.7 54.7H15.5C15.2 54.7 15.1 55 15.2 55.2C17.4 57.4 21.9 61.9 24.2 64.2C24.8 64.8 25.6 65.3 26.6 65.3H36.4C37.3 65.3 38.1 64.5 38.1 63.6V40.7C38.1 40.4 38.3 40.2 38.6 40.2H39.7C50.7 40.2 61.4 33.5 61.4 20.1C61.6 8.1 53.4 0 39.9 0Z" fill="url(#paint0_linear)"/>
            <defs>
            <linearGradient id="paint0_linear" x1="0" y1="32.6588" x2="61.6149" y2="32.6588" gradientUnits="userSpaceOnUse">
            <stop offset="0.3968" stop-color="#772B8F"/>
            <stop offset="1" stop-color="#EC1D7F"/>
            </linearGradient>
            </defs>
          </svg>
          <h2 class="form__title" style="text-transform:uppercase;">Site is temporarily unavailable</h2>

          <div style="font-size:125%;">
            <p>
              We&rsquo;re performing some maintenance at the moment. If you need to you can always <a href="mailto:barcreports@teleperformance.com">contact us</a>, otherwise repairs are in progress and we&rsquo;ll be back online shortly!
            </p>
            <p style="margin-top:1.5rem;">
              We apologize for any inconvenience.
            </p>
          </div>
        </form>
      </div>
    </div>
    <div class="container__panels">
      <div class="panel panel__left">
        <img class="panel__image" src="<?php echo _DR_.'/dist/img/maintenance.svg'?>" alt="" />
      </div>
    </div>
  </div>

</body>
<script>
</script>
</html>