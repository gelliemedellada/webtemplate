<section class="content">
  <div class="error-page">
    <h1 class="headline text-danger mt-3">
      <i class="fas fa-exclamation-triangle text-danger"></i>
    </h1>
    <div class="error-content">
    <h1 style="font-weight:900;text-transform:uppercase;">
      Error
    </h1>
      <p class="pt-3 pb-3 text-lg">
      The requested operation could not continue.
      You may <a href="<?php echo _DR_ ?>" class="text-custom">return to the home page</a> or 
      <a href="mailto:barcreports@teleperformance.com" class="text-custom">contact us</a> for further assistance.
      </p>
    </div>
  </div>
</section>