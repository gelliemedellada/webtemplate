<?php

function getLocalSettings($name){
  $DBA = new PDO('sqlite:'.__DIR__.'/localDB.sqlite');
  $getLocalSettings = $DBA->query("SELECT value FROM site_config WHERE name = '{$name}'")->fetch(PDO::FETCH_ASSOC);
  return $getLocalSettings['value'];
}

function getPageAccess($page, $dir){
  $DBA = new PDO('sqlite:'.__DIR__.'/localDB.sqlite');
  $getPageAccess = $DBA->query("SELECT access FROM page_config WHERE page = '{$page}' AND dir = '{$dir}'")->fetch(PDO::FETCH_ASSOC);
  return $getPageAccess['access'];
}

$cfg 								= array();
$cfg["maintenance"]	= getLocalSettings('maintenance');
$cfg["version"]			= getLocalSettings('version');



